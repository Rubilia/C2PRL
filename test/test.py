import gym

env = gym.make('Breakout-ram-v0')

s = env.reset()

A = env.action_space

for _ in range(1000):
    s, _, done, _ = env.step(A.sample())
    env.render()
    if done:
        env.reset()
