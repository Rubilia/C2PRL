from typing import Tuple

import gym
import tensorflow as tf
import numpy as np
from Executor.agent import Agent
from Executor.environment import ExecutorEnvironment
from Executor.state_representer import Representer
from Executor.utils import STATE_REPRESENTATION, S_DIM, A_DIM, R_DIM


class GoalEnvironment(object):
    def __init__(self, env_name: str, render=False, restore=True):
        self.env_name = env_name
        self.env = gym.make(env_name)
        self.s = self.env.reset()
        # Executor config
        self.executor_env = ExecutorEnvironment(env_name, gym_env=self.env, render=render, wait=render)
        sess = tf.Session()
        if STATE_REPRESENTATION:
            compressor = Representer(sess, S_DIM, R_DIM)
        else: compressor = None
        self.agent = Agent(sess=sess, env=self.executor_env, compressor=compressor)
        sess.run(tf.global_variables_initializer())
        if restore:
            self.agent.restore('data/models/pretrain/executor')
            if STATE_REPRESENTATION:
                compressor.restore('data/models/pretrain/compressor')
        self.LOW = np.array([self.executor_env.goal_space[x][0] for x in range(self.executor_env.s_dim)][2:])
        self.HIGH = np.array([self.executor_env.goal_space[x][1] for x in range(self.executor_env.s_dim)][2:])

    def reset(self):
        return self.executor_env.reset()

    def step(self, goal) -> Tuple[np.ndarray, float, bool, int]:
        self.agent.test = True
        g = np.copy(goal.flatten())
        if self.env_name == 'Pendulum-v0' and not STATE_REPRESENTATION:
            g = np.array([np.cos(g[0]), np.sin(g[0]), g[1]])
        elif self.env_name == 'Acrobot-v1':
            G = np.zeros(6)
            G[0] = np.cos(g[0])
            G[1] = np.sin(g[0])
            G[2] = np.cos(g[1])
            G[3] = np.sin(g[1])
            G[4] = g[2]
            G[5] = g[3]
            g = G
        self.s, r, done, steps = self.agent.reach_goal(self.s, g)
        r /= steps
        r += 1.
        return self.s, r, done, steps

    def set_render(self, render: bool):
        self.executor_env.render = render
