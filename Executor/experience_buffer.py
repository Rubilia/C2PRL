import random
import time
from typing import Tuple
import numpy as np

from Executor.utils import PART_TO_REMOVE

USE_ASAMPLING = True
A_percentage = 0.1
Statistics = 100000


class ReplayBuffer(object):

    def __init__(self, max_size=10 ** 5, max_positive=10 ** 5, batch_size=1024, s_dim=0, a_dim=0):
        self.max_size = max_size
        self.max_positive_size = max_positive
        self.data = []
        self.batch_size = batch_size
        self.s_dim = s_dim
        self.positive_data = []
        self.positives_data = []
        self.counter = 0
        self.a_dim = a_dim

    def __len__(self):
        return len(self.data)

    def add(self, experience):
        if experience[2] == 0.:
            self.positive_data.append(
                (experience[0], experience[1].flatten(), experience[2], experience[3], experience[4], experience[5]))
            self.positives_data.append(1.)
        else:
            self.data.append(
                (experience[0], experience[1].flatten(), experience[2], experience[3], experience[4], experience[5]))
            self.positives_data.append(0.)
        if len(self.positives_data) > Statistics:
            self.positives_data = self.positives_data[Statistics // PART_TO_REMOVE:]

        if len(self) >= self.max_size:
            beg_index = self.max_size // PART_TO_REMOVE
            self.data = self.data[beg_index:]
        if len(self.positive_data) >= self.max_positive_size:
            self.positive_data = self.positive_data[self.max_positive_size // PART_TO_REMOVE:]

    def get_batch(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        # sample data
        perc = min(.9, max(A_percentage, np.mean(self.positives_data)))
        negative = random.sample(self.data, int(self.batch_size * (1 - perc)))
        positive = random.sample(self.positive_data, min(int(self.batch_size * perc),
                                                         len(self.positive_data)))
        negative.extend(positive)
        random.shuffle(negative)
        sample = np.column_stack(negative)

        s = np.hstack(sample[0]).reshape((-1, self.s_dim))
        a = np.hstack(sample[1]).reshape((-1, self.a_dim))
        r = np.hstack(sample[2])
        s_ = np.hstack(sample[3]).reshape((-1, self.s_dim))
        g = np.hstack(sample[4]).reshape((-1, self.s_dim))
        done = np.hstack(sample[5])

        return s, a, r, s_, g, done
