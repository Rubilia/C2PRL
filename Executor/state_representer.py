import tensorflow as tf
import numpy as np
import random
from tensorflow.python.keras.layers import Dense

from Executor.utils import get_executor_config, PART_TO_REMOVE, DEFAULT_DEVICE


class Representer(object):
    def __init__(self, sess, s_dim: int, r_dim: int, scope='representer', lr=1e-4, max_storage_len=10 ** 7, batch_size=1024 * 2):
        self.sess: tf.Session = sess
        self.max_size = max_storage_len
        self.batch_size = batch_size
        self.scope = scope
        self.S_DIM = s_dim
        self.REPR_DIM = r_dim
        self.S = tf.placeholder(tf.float32, [None, s_dim], name='state')
        self.encoder = self.create_encoder(self.S, scope=scope + 'encoder')
        self.decoder = self.create_decoder(self.encoder, scope=scope + 'decoder')
        self.loss = tf.losses.huber_loss(self.decoder, self.S)
        self.optimizer = tf.train.AdamOptimizer(lr).minimize(self.loss)
        self.weights_encoder = list(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.scope + 'encoder'))
        self.weights_decoder = list(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.scope + 'decoder'))
        self.storage: list = []
        self.add_new_data = False

    def create_encoder(self, S, scope: str):
        neurons = get_executor_config('state_compressor_encoder')
        neurons = [self.S_DIM] + neurons + [self.REPR_DIM]
        output = S
        with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
            for id, n in enumerate(neurons[1:-1]):
                weight_init = tf.random_uniform_initializer(minval=-1 / neurons[id] ** 0.5,
                                                            maxval=1 / neurons[id] ** 0.5)
                bias_init = tf.random_uniform_initializer(minval=-1 / neurons[id] ** 0.5,
                                                          maxval=1 / neurons[id] ** 0.5)
                output = Dense(n, activation='relu', kernel_initializer=weight_init, bias_initializer=bias_init)(output)
            output = Dense(neurons[-1], activation='tanh',
                           kernel_initializer=tf.random_uniform_initializer(minval=-3e-3, maxval=3e-3),
                           bias_initializer=tf.random_uniform_initializer(minval=-3e-3, maxval=3e-3))(output)
        return output

    def create_decoder(self, S, scope: str):
        neurons = get_executor_config('state_compressor_decoder')
        neurons = [self.REPR_DIM] + neurons + [self.S_DIM]
        output = S
        with tf.variable_scope(scope):
            for id, n in enumerate(neurons[1:-1]):
                weight_init = tf.random_uniform_initializer(minval=-1 / neurons[id] ** 0.5,
                                                            maxval=1 / neurons[id] ** 0.5)
                bias_init = tf.random_uniform_initializer(minval=-1 / neurons[id] ** 0.5,
                                                          maxval=1 / neurons[id] ** 0.5)
                output = Dense(n, activation='relu', kernel_initializer=weight_init, bias_initializer=bias_init)(output)
            output = Dense(neurons[-1], activation='tanh',
                           kernel_initializer=tf.random_uniform_initializer(minval=-3e-3, maxval=3e-3),
                           bias_initializer=tf.random_uniform_initializer(minval=-3e-3, maxval=3e-3))(output)
        return output

    def compress_state(self, s: np.ndarray):
        return self.sess.run(self.encoder, feed_dict={self.S: s.reshape((-1, self.S_DIM))})

    def add_data(self, s):
        if len(self.storage) > self.max_size:
            self.storage = self.storage[len(self.storage) // PART_TO_REMOVE:]
        self.storage.append(s)

    def sample_batch(self):
        # sample data
        s = np.hstack(random.choices(self.storage, k=self.batch_size))
        return np.copy(s.reshape((-1, self.S_DIM)))

    def learn(self, iterations) -> float:
        if len(self.storage) < self.batch_size: return 0.

        S = self.sample_batch()
        Loss = 0.
        for _ in range(iterations):
            loss, _ = self.sess.run([self.loss, self.optimizer], feed_dict={self.S: S})
            Loss += loss
        Loss /= iterations
        return Loss

    def evaluate(self, iterations) -> float:
        if len(self.storage) < self.batch_size: return 0.

        Loss = 0.
        for _ in range(iterations):
            S = self.sample_batch()
            loss = self.sess.run(self.loss, feed_dict={self.S: S})
            Loss += loss
        Loss /= iterations
        return Loss

    def update_encoder(self, params):
        ops = []
        for param1, param2 in zip(params, self.weights_encoder):
            ops.append(tf.assign(param2, param1))

        with tf.device(DEFAULT_DEVICE):
            self.sess.run(ops)

    def update_decoder(self, params):
        ops = []
        for param1, param2 in zip(params, self.weights_decoder):
            ops.append(tf.assign(param2, param1))

        with tf.device(DEFAULT_DEVICE):
            self.sess.run(ops)

    def save(self, path: str):
        encoder = self.sess.run(self.weights_encoder)
        decoder = self.sess.run(self.weights_encoder)
        np.save(path + '_ecoder', encoder, allow_pickle=True)
        np.save(path + '_deoder', decoder, allow_pickle=True)

    def restore(self, path: str):
        path += '_actor.npy'
        encoder = np.load(path + '_encoder.npy', allow_pickle=True)
        decoder = np.load(path + '_decoder.npy', allow_pickle=True)
        self.update_encoder(encoder)
        self.update_decoder(decoder)

