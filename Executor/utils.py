import types
from typing import Tuple
import numpy as np
from tensorflow.python.client import device_lib


def configure_training(env: str) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, int, types.LambdaType]:
    """
    :param env: name of the environment :rtype: tuple consists of goal_space, highest_action, lowest_action, noise,
    max_steps_per_episode, state_function_preprocessor
    """
    if env == 'MountainCarContinuous-v0':
        goal_space = np.array([[-1.2, .6], [-.07, .07]])
        highest_action = np.array([1.])
        lowest_action = np.array([-1.])
        noise = np.array([.05])
        max_steps_per_episode = 500
        return goal_space, highest_action, lowest_action, noise, max_steps_per_episode, lambda x: x
    elif env == 'Pendulum-v0':
        goal_space = np.array([[-1., 1.], [-1., 1.], [-1., 1.]])
        highest_action = np.array([1.])
        lowest_action = np.array([-1.])
        noise = np.array([.05])
        max_steps_per_episode = 400
        return goal_space, highest_action, lowest_action, noise, max_steps_per_episode, lambda x: x
    elif env == 'Acrobot-v1':
        goal_space = np.array([[-1, 1], [-1, 1], [-1, 1], [-1, 1], [-np.pi, np.pi], [-np.pi, np.pi]])
        highest_action = np.array([1., 1., 1.])
        lowest_action = np.array([-1., -1., -1.])
        noise = np.array([.1])
        max_steps_per_episode = 500
        return goal_space, highest_action, lowest_action, noise, max_steps_per_episode, lambda x: x
    elif 'CartPole' in env:
        goal_space = np.array([[-1, 1], [-.2, .2], [-np.deg2rad(12), np.deg2rad(12)], [-.1, .1]])
        highest_action = np.array([1., 1.])
        lowest_action = np.array([-1., -1.])
        noise = np.array([.1])
        max_steps_per_episode = 200
        return goal_space, highest_action, lowest_action, noise, max_steps_per_episode, lambda x: x
    elif env == 'Breakout-ram-v0':
        goal_space = np.array([[-1, 1] for _ in range(128)])
        highest_action = np.array([1., 1., 1., 1.])
        lowest_action = np.array([-1., -1., -1., -1.])
        noise = np.array([.1])
        max_steps_per_episode = 200
        return goal_space, highest_action, lowest_action, noise, max_steps_per_episode, lambda x: (x - 128.) / 128.


# Contains networks configurations
def get_executor_config(subj: str):
    if subj == 'actor_network':
        return [64, 128, 64]
    elif subj == 'critic_network':
        return [64, 128, 64]
    elif subj == 'state_compressor_encoder':
        return [128, 64, 32]
    elif subj == 'state_compressor_decoder':
        return [32, 64, 128]
    elif subj == 'q_network':
        return [64, 64, 32]


def is_goal_reached(s, g):
    return np.linalg.norm(s - g) <= THRESHOLD

# def is_goal_reached(s, g):
#     for S, G, Thr in zip(s, g, THRESHOLD):
#         if abs(S - G) > Thr: return False
#     return True


def process_done(done: bool, env) -> bool:
    if env.env_name == 'Pendulum-v0':
        return env.step_counter >= env.max_steps
    elif env.env_name == 'MountainCarContinuous-v0':
        return done or env.step_counter < env.max_steps
    elif env.env_name == 'Acrobot-v1':
        return env.step_counter >= env.max_steps or done
    elif 'CartPole' in env.env_name:
        return env.step_counter >= env.max_steps or done
    elif env.env_name == 'Breakout-ram-v0':
        return env.step_counter >= env.max_steps or done


def get_available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']


# Agent config (does not depend on environment)

PART_TO_REMOVE = 8

TIME_BTWN_FRAMES = .02

# Agent config (depends on environment)

RANDOM_ACTIONS_PERCENT = .2

GOALS_TO_REPLAY = 3

# THRESHOLD = [.2, .2, .1, .1]

THRESHOLD = .64

SHOW_GOALS = False

DHER_BATCH = 64

STATE_REPRESENTATION = True

R_DIM = 3

S_DIM = 128

A_DIM = 4

DEFAULT_DEVICE = 'cpu:0'

USE_MULTI_GPU = False
