import os
from threading import Thread

import numpy as np
import tensorflow as tf

from Executor.agent import Agent
from Executor.environment import ExecutorEnvironment
from Executor.state_representer import Representer
from Executor.utils import R_DIM, STATE_REPRESENTATION, S_DIM, A_DIM, get_available_gpus

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

MAX_STEPS = 50000
TEST_FREQ = 40
SAVE_FREQ = 100
TEST_EPISODES = 100
ENV_NAME = 'Breakout-ram-v0'
GLOBAL_STEP = 0
NUM_THREADS = 2
AC_AGENT = True
INITIAL_DATA_COLLECTION_ITEARATIONS = 1000


def get_thread(agent, compressor):
    def thread():
        return Thread(target=agent.train, args=(compressor,))
    return thread


def test(agent, compressor=None):
    agent.test = True
    goal_counter = 0
    for episode in range(TEST_EPISODES):
        success, _ = agent.train(compressor=compressor)
        goal_counter += success
    goal_counter /= TEST_EPISODES
    print(
        '########################\nTesting agent: avg goals reached in episode: %f\n########################' % goal_counter)
    agent.test = False


def collect_data(threads: list):
    T = []
    for t in threads:
        T.append(t())
        T[-1].start()

    for t in T:
        t.join()


def run_training(agent: Agent, Agents, threads, compressor=None):
    global GLOBAL_STEP
    agent.test = False
    print('Collecting initial data')
    # Collect data
    for _ in range(INITIAL_DATA_COLLECTION_ITEARATIONS):
        collect_data(threads)

    print('Initial data collected, learning begins')

    while GLOBAL_STEP < MAX_STEPS:
        GLOBAL_STEP += 1
        step = GLOBAL_STEP
        # Save agent with given frequency
        if GLOBAL_STEP % SAVE_FREQ == 0 and GLOBAL_STEP > 0:
            # Save agent
            path = os.getcwd()
            path = path[:-(len(os.path.basename(path)) + 1)]
            path += '/data/models/executor/executor_' + str(step) + '/'
            if not os.path.isdir(path):
                os.mkdir(path)
            agent.save(path + 'executor')
            agent.restore(path + 'executor')
            print('Executor network saved')
            # Save representation network if in use
            if STATE_REPRESENTATION:
                path = os.getcwd()
                path = path[:-(len(os.path.basename(path)) + 1)]
                path += '/data/models/compressor/compressor' + str(step) + '/'
                if not os.path.isdir(path):
                    os.mkdir(path)
                compressor.save(path + 'compressor')
                print('\nRepresenter saved')
        # Test performance every TEST_FREQ steps
        if GLOBAL_STEP % (TEST_FREQ // NUM_THREADS) == 0 and GLOBAL_STEP > 0:
            test(agent)

        # if STATE_REPRESENTATION:
        #     for _ in range(16):
        #         compressor.learn(16)

        collect_data(threads)

        success, data = agent.train(compressor)
        # Update target weights
        for A in Agents:
            A.set_params(agent)

        print('\n%f%% - reached %d goals during episode' % (100 * step / MAX_STEPS, success))
        if not data == '':
            print(data)


def compressor_data_collector(compressor, episodes, env):
    for i in range(episodes):
        done = False
        s = env.reset()
        compressor.add_data(s)
        while not done:
            a = np.random.uniform(env.low, env.high)
            s, _, done = env.step(a)
            compressor.add_data(s)


render = False
env = ExecutorEnvironment(ENV_NAME, render=render)
if STATE_REPRESENTATION:
    sess = tf.Session()
    compressor = Representer(sess, S_DIM, R_DIM)
    sess.run(tf.global_variables_initializer())
else:
    compressor = None

# Create global agents
sess = tf.Session()
GLOBAL_AGENT = Agent(sess=sess, env=env, compressor=compressor, scope='main', batch_size=1024,
                     GLOBAL=True, device='gpu:0', buffer_size=10**6, is_AC=AC_AGENT)

sess.run(tf.global_variables_initializer())


try:
    path = os.getcwd()
    path = path[:-(len(os.path.basename(path)) + 1)]
    path += '/data/models/pretrain/executor'
    GLOBAL_AGENT.restore(path)
    print('Executor model restored')
except Exception as e:
    print('Executor restoration failed')

# restore compressor
if STATE_REPRESENTATION:
    try:
        path = os.getcwd()
        path = path[:-(len(os.path.basename(path)) + 1)]
        path += '/data/models/pretrain/compressor'
        compressor.restore(path)
        print('\nCompressor model restored\n')
    except Exception:
        print('\nCompressor restoration failed\n')

        # pretrain compressor on random data
        compressor_data_collector(compressor, 2048, env)
        for _ in range(4096):
            loss = compressor.learn(16)
            print(loss)
        print('Starting evaluation of compressor')
        print('Avg loss over dataset: %f' % compressor.evaluate(512))
        print('Compressor successfully pretrained\n')

# Create parallel agents
Agents = []
Threads = []

devices = get_available_gpus() + ['cpu:0']


for i in range(NUM_THREADS):
    # Instantiate agent with its own environment
    sess = tf.Session()
    agent = Agent(sess=sess, env=ExecutorEnvironment(ENV_NAME, render=False), buffer=GLOBAL_AGENT.buffer,
                  scope='thread_%d_' % i, device=devices[i % max(1, len(devices))], is_AC=AC_AGENT, compressor=compressor)
    sess.run(tf.global_variables_initializer())
    agent.set_params(GLOBAL_AGENT)
    Agents.append(agent)
    # Create Thread for agent
    Threads.append(get_thread(agent, compressor))

run_training(agent=GLOBAL_AGENT, Agents=Agents, threads=Threads, compressor=compressor)
